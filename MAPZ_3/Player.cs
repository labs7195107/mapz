﻿namespace MAPZ_3;

public class Player
{
    public string Name { get; set; }
    public int Score { get; set; }

    public Player(int score, string name)
    {
        this.Score = score;
        this.Name = name;
    }

    public Player()
    {

    }

    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType()) return false;
        Player other = obj as Player;
        if (this.Name.Equals(other.Name) && this.Score == other.Score)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
﻿namespace MAPZ_3;

public enum BuildingType
{
    Military,
    Research,
    Civil
}
﻿using System.Security.Cryptography;

namespace MAPZ_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Player me = new Player(0, "Yura");
            Player evilPlayer = new Player(0, "");
            Model model = new Model();
            model.AddBuilding(new Building(me, BuildingType.Civil,
                "My first building", "Home", -1));
            model.AddBuilding(new Building(me, BuildingType.Military,
                "My second building", "Base", 100));
            model.AddBuilding(new Building(me, BuildingType.Civil,
                "My third building", "Windmill", 300));
            model.AddBuilding(new Building(evilPlayer, BuildingType.Research,
                "My fourth building", "University", 200));
            model.AddBuilding(new Building(evilPlayer, BuildingType.Military,
                "My fifth building", "Home", 400));
            model.AddBuilding(new Building(me, BuildingType.Research,
                "My sixth building", "Base", 100));
            model.AddBuilding(new Building(evilPlayer, BuildingType.Research,
                "My seventh building", "Dorm", 10));
            model.AddBuilding(new Building(me, BuildingType.Military,
                "My eighth building", "Gunsmith", 600));
            model.AddBuilding(new Building(evilPlayer, BuildingType.Military,
                "My nineth building", "barrack", 330));
            model.AddBuilding(new Building(me, BuildingType.Civil,
                "My tenth building", "Home", 220));
            model.AddBuilding(new Building(evilPlayer, BuildingType.Civil,
                "My eleventh building", "TownHall", 1100));
            int hello = 0;
            Console.WriteLine(hello = 1);
        }
    }
}

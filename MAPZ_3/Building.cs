﻿namespace MAPZ_3;

public class Building
{
    public Player Owner { get; set; }
    public string Name { get;}
    public string Description { get;}
    public BuildingType BuildingType { get;}
    public int HealthPoints { get; protected set; }
    public Building(Player owner, BuildingType buildingType, string description, string name, int healthPoints)
    {
        this.BuildingType = buildingType;
        this.Description = description;
        this.Name = name;
        this.HealthPoints = healthPoints;
        this.Owner = owner;
    }

    public override string ToString()
    {
        return string.Format("{0} {1} {2} ", Owner, Name, BuildingType);
    }

    public override bool Equals(object? obj)
    {
        if(obj == null || GetType() != obj.GetType()) return false;
        Building other = obj as Building;
        if (this.BuildingType == other.BuildingType &&
            this.Name.Equals(other.Name) &&
            this.Owner.Equals(other.Owner) &&
            this.Description.Equals(other.Description))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
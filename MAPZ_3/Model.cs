﻿using System.Collections;

namespace MAPZ_3;

public class Model
{
    public List<Building> Buildings { get; }

    public Model()
    {
        this.Buildings = new List<Building>();
    }

    public void AddBuilding(Building toAdd)
    {
        Buildings.Add(toAdd);
    }

    public void RemoveBuilding(Building toRemove)
    {
        Buildings.Remove(toRemove);
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        Model other = (Model)obj;

        if (Buildings.Count != other.Buildings.Count)
        {
            return false;
        }

        return this.Buildings.TrueForAll(building => other.Buildings.Count(otherBuilding=> otherBuilding == building) == 1);
    }
}

public static class Utilities
{
    public static Model ToModel(this List<Building> inputBuildings)
    {
        Model model = new Model();
        inputBuildings.ForEach(building => model.AddBuilding(building));
        return model;
    }

    public static bool isArraySortedAscending(Building[] array)
    {
        for (int i = 0; i < array.Length-1; i++)
        {
            if (array[i].HealthPoints > array[i + 1].HealthPoints)
            {
                return false;
            }
        }
        return true;
    }
}
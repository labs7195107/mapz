using System.Collections;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using FluentAssertions;
using MAPZ_3;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace LINQTests
{
    public class UnitTest1
    {
        private Model _model = new();
        private Player _me;
        private Player _evilPlayer;
        private Dictionary<Player, List<Building>> dictionary;

        public UnitTest1()
        {
            _me = new Player(0, "Yura");
            _evilPlayer = new Player(0, "");

            _model = new Model();

            {
                _model.AddBuilding(new Building(_me, BuildingType.Civil,
                    "My first building", "Home", -1));
                _model.AddBuilding(new Building(_me, BuildingType.Military,
                    "My second building", "Base", 100));
                _model.AddBuilding(new Building(_me, BuildingType.Civil,
                    "My third building", "Windmill", 300));
                _model.AddBuilding(new Building(_evilPlayer, BuildingType.Research,
                    "My fourth building", "University", 200));
                _model.AddBuilding(new Building(_evilPlayer, BuildingType.Military,
                    "My fifth building", "Home", 400));
                _model.AddBuilding(new Building(_me, BuildingType.Research,
                    "My sixth building", "Base", 100));
                _model.AddBuilding(new Building(_evilPlayer, BuildingType.Research,
                    "My seventh building", "Dorm", 10));
                _model.AddBuilding(new Building(_me, BuildingType.Military,
                    "My eighth building", "Gunsmith", 600));
                _model.AddBuilding(new Building(_evilPlayer, BuildingType.Military,
                    "My nineth building", "barrack", 330));
                _model.AddBuilding(new Building(_me, BuildingType.Civil,
                    "My tenth building", "Home", 220));
                _model.AddBuilding(new Building(_evilPlayer, BuildingType.Civil,
                    "My eleventh building", "TownHall", 1100));
            } //Adding some buildings

            dictionary = new Dictionary<Player, List<Building>>();

            new List<Player>() { _me, _evilPlayer }.ForEach(player => dictionary.Add(player, _model.Buildings.Where(building => building.Owner == player).ToList()));
        }

        [Fact]
        public void SelectListTest()
        {
            var result = _model.Buildings.Select(building => building.Owner).ToList();
            result.Should().NotBeNullOrEmpty();
            foreach (var player in result)
            {
                player.Should().BeOneOf(new List<Player>() { _me, _evilPlayer });
            }
        }

        [Fact]
        public void WhereListTest()
        {
            var result = _model.Buildings.Where(building => building.BuildingType == BuildingType.Civil).ToList();
            result.Should().NotBeNullOrEmpty();
            foreach (var building in result)
            {
                building.BuildingType.Should().Be(BuildingType.Civil);
            }
        }

        [Fact]
        public void AnonymousTypesTest()
        {
            var builingQuery = from building in _model.Buildings
                where building.Owner == _evilPlayer && building.BuildingType == BuildingType.Research
                select new { Owner = building.Owner, BuildingType = building.BuildingType };
            builingQuery.Should().NotBeNullOrEmpty();
            foreach (var objects in builingQuery)
            {
                objects.Owner.Should().Be(_evilPlayer);
                objects.BuildingType.Should().Be(BuildingType.Research);
            }
        }

        [Fact]
        public void ExtensionMethodTest()
        {
            List<Building> buildings = _model.Buildings;
            Model testModel = buildings.ToModel();

            testModel.Equals(_model).Should().BeTrue();
        }

        [Fact]
        public void ConvertListToArrayTest()
        {
            Building first = _model.Buildings[0];
            Building second = _model.Buildings[1];
            Building third = _model.Buildings[2];

            Building[] arrayBuildings = { first, second, third };
            
            List<Building> listToTest = new List<Building>();
            
            foreach (var building in arrayBuildings)
            {
                listToTest.Add(building);
            }

            Enumerable.SequenceEqual(listToTest.ToArray(), arrayBuildings).Should().BeTrue();
        }



        [Fact]
        public void SortingHpTest()
        {
            Building[] arrayBuildings = new Building[_model.Buildings.Count];
            arrayBuildings = _model.Buildings.ToArray();
            Array.Sort(arrayBuildings, (building, building1) => building.HealthPoints.CompareTo(building1.HealthPoints));
            Utilities.isArraySortedAscending(arrayBuildings).Should().BeTrue();
        }

        [Fact]
        public void HardOperationTest()
        {
            var result = _model.Buildings.GroupBy(building => building.Owner).ToDictionary((building) => building.Key, building => building.OrderBy((building) => building.HealthPoints));
            foreach (var some  in result)
            {
                dictionary[some.Key].TrueForAll(building => some.Value.Count(anotherBuilding => building == anotherBuilding)==1).Should().BeTrue();
            }
        }
        [Fact]
        public void SortingTest()
        {
            var result = 
                from building in _model.Buildings 
                where building.Owner != _me
                orderby building.HealthPoints ascending 
                select building;

            var listResult = result.ToList();
            for (int i = 0; i < listResult.Count-1; i++)
            {
                (listResult[i].HealthPoints < listResult[i + 1].HealthPoints).Should().BeTrue();
            }
        }
        [Fact]
        public void initializerTest()
        {

            List<Building> buildings = new List<Building>{new (_me, BuildingType.Civil, "Some", "Hello", 20)};
            Building building2 = new Building(_me, BuildingType.Civil, "", "DS", 500);
            var player = new Player { Score = 0, Name = "Yura" };
            player.Name.Should().Be("Yura");
            player.Score.Should().Be(0);
        }
    }
}